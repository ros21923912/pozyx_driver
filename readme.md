# Pozyx driver
### setup.py
The setup.py file is a Python script that is used to configure and install a ROS2 package. It defines the following information about the package:

    The name of the package
    The version of the package
    The list of packages that the package depends on
    The list of files that are included in the package
    The entry points for the package

In the setup.py file you provided, the following information is defined:

    The name of the package is pozyx_driver.
    The version of the package is 0.1.0.
    The package depends on the setuptools package.
    The package includes the following files:
        The pozyx_driver package itself, which contains the Python code for the ROS2 Pozyx driver.
        The package.xml file, which contains metadata about the package.
        The resource/pozyx_driver file, which is a resource file that is used by the ROS2 build system.
    The package has a single entry point, called pozyxNode. This entry point is a console script that is used to launch the ROS2 Pozyx driver node.

The setup.py file is used by the ROS2 build system to install the package. When you run the following command:

ros2 pkg install pozyx_driver

the ROS2 build system will use the setup.py file to download the package, extract the files, and install the package.
### package.xml
The package.xml file is a metadata file that is used to describe a ROS2 package. It defines the following information about the package:

    The name of the package
    The version of the package
    The description of the package
    The maintainers of the package
    The license for the package
    The dependencies of the package
    The test dependencies of the package
    The exports of the package

In the package.xml file you provided, the following information is defined:

    The name of the package is pozyx_driver.
    The version of the package is 0.0.0.
    The description of the package is TODO: Package description.
    The maintainer of the package is Guelakais.
    The license for the package is Apache2.
    The dependencies of the package are:
        time
        sensor_msgs
        pypozyx
        geometry_msgs
        rclpy
        numpy
    The test dependencies of the package are:
        ament_copyright
        ament_flake8
        ament_pep257
        python3-pytest
    The exports of the package are:
        The build type, which is ament_python.

The package.xml file is used by the ROS2 build system to build and install the package. When you run the following command:

ros2 pkg build pozyx_driver

the ROS2 build system will use the package.xml file to download the dependencies of the package, build the package, and install the package.
#### pozyxNode.py
The pozyxNode.py file is the main file for the ROS2 Pozyx driver. It contains the following code:

    Import statements for the necessary ROS2 and Python libraries.
    A class called EnvironmentConstants, which defines the constants that are used by the ROS2 Pozyx driver.
    A class called PozyxPublisher, which implements the ROS2 publisher for the Pozyx device.
    A function called main(), which is the main entry point for the ROS2 Pozyx driver.

The EnvironmentConstants class defines the following constants:

    remoteId: The ID of the Pozyx device that the ROS2 Pozyx driver will be communicating with.
    height: The height of the robot.
    anchors: A list of the anchor coordinates that the ROS2 Pozyx driver will use to calculate the position of the robot.
    algorithm: The positioning algorithm that the ROS2 Pozyx driver will use.
    dimension: The dimension of the positioning space.

The PozyxPublisher class implements the ROS2 publisher for the Pozyx device. It has the following methods:

    __init__(): The constructor for the PozyxPublisher class. It initializes the ROS2 node, the Pozyx device, and the ROS2 publishers for the IMU and position data.
    setup(): The setup method for the PozyxPublisher class. It sets up the IMU and position publishers.
    publish(): The publish method for the PozyxPublisher class. It publishes the IMU and position data to the ROS2 topics.

The main() function is the main entry point for the ROS2 Pozyx driver. It does the following:

    Performs a version check on the Pozyx library.
    Initializes the ROS2 node.
    Creates a PozyxPublisher object.
    Sets up the PozyxPublisher object.
    Enters a loop where it publishes the IMU and position data to the ROS2 topics.
    Destroys the PozyxPublisher object.
    Shutdowns the ROS2 node.

### pozyxSensorData
pozyxSensorData is the library that contains the various functions provided by the driver. Each module here is responsible for a publisher. Currently it provides the /robot_position and /pozyx_Imu messages.
#### orientation.py
The orientation.py file contains the code for the Orientation3D class, which is responsible for publishing the IMU data from the Pozyx device to a ROS2 topic.

The Orientation3D class has the following methods:

    __init__(): The constructor for the Orientation3D class. It initializes the Pozyx device, the ROS2 node, and the ROS2 publisher for the IMU data.
    loop(): The loop method for the Orientation3D class. It continuously checks for new IMU data from the Pozyx device and publishes it to the ROS2 topic if there is any new data.
    publishSensorData(): The publishSensorData method for the Orientation3D class. It publishes the IMU data to the ROS2 topic.

The loop() method first checks if there is any new IMU data from the Pozyx device by calling the checkForFlag() method. If there is new IMU data, the loop() method calls the getAllSensorData() method to get the IMU data from the Pozyx device. The getAllSensorData() method returns a SensorData object, which contains the IMU data. The loop() method then calls the getCalibrationStatus() method to get the calibration status of the IMU from the Pozyx device. The getCalibrationStatus() method returns a SingleRegister object, which contains the calibration status. The loop() method then checks if the IMU data is valid and the calibration status is good. If the IMU data is valid and the calibration status is good, the loop() method calls the publishSensorData() method to publish the IMU data to the ROS2 topic.

The publishSensorData() method first creates a new Imu message. The Imu message is a ROS2 message that is used to represent IMU data. The publishSensorData() method then sets the timestamp of the Imu message to the current time. The publishSensorData() method then sets the orientation, angular velocity, and linear acceleration of the Imu message to the IMU data from the Pozyx device. The publishSensorData() method then publishes the Imu message to the ROS2 topic.

#### ready_to_localize.py
The ready_to_localize.py file contains the code for the RobotPosition class, which is responsible for calculating the position of the robot using the Pozyx device.

The RobotPosition class has the following methods:

    __init__(): The constructor for the RobotPosition class. It initializes the Pozyx device, the ROS2 node, the anchors, and the ROS2 publisher for the robot position.
    setup(): The setup method for the RobotPosition class. It sets up the anchors and publishes the anchor configuration to the ROS2 topic.
    loop(): The loop method for the RobotPosition class. It continuously calculates the position of the robot and publishes it to the ROS2 topic.
    printPublishPosition(): The printPublishPosition method for the RobotPosition class. It prints and publishes the position of the robot to the console and the ROS2 topic.
    printPublishErrorCode(): The printPublishErrorCode method for the RobotPosition class. It prints and publishes the error code of the last operation to the console and the ROS2 topic.
    setAnchorsManual(): The setAnchorsManual method for the RobotPosition class. It sets the anchors manually.
    printPublishConfigurationResult(): The printPublishConfigurationResult method for the RobotPosition class. It prints and publishes the result of the anchor configuration to the console and the ROS2 topic.
    printPublishAnchorConfiguration(): The printPublishAnchorConfiguration method for the RobotPosition class. It prints and publishes the anchor configuration to the console and the ROS2 topic.

The setup() method first sets the anchors using the setAnchorsManual() method. Then, it publishes the anchor configuration to the ROS2 topic using the printPublishAnchorConfiguration() method.

The loop() method continuously calculates the position of the robot using the doPositioning() method. The doPositioning() method returns the position of the robot in millimeters. The loop() method then prints and publishes the position of the robot to the console and the ROS2 topic using the printPublishPosition() method.

The printPublishPosition() method first prints the position of the robot to the console. Then, it publishes the position of the robot to the ROS2 topic. The ROS2 topic is a message that is used to represent the position of the robot.

The printPublishErrorCode() method first prints the error code of the last operation to the console. Then, it publishes the error code of the last operation to the ROS2 topic. The ROS2 topic is a message that is used to represent the error code of the last operation.

The setAnchorsManual() method sets the anchors manually. The method first clears the devices in the Pozyx device. Then, it adds the anchors to the Pozyx device. Finally, it sets the selection of anchors to automatic.

The printPublishConfigurationResult() method prints and publishes the result of the anchor configuration to the console and the ROS2 topic. The method first gets the list size of the devices in the Pozyx device. Then, it gets the device IDs of the anchors. Finally, it gets the coordinates of the anchors.

The printPublishAnchorConfiguration() method prints and publishes the anchor configuration to the console and the ROS2 topic. The method first prints the anchor configuration to the console. Then, it publishes the anchor configuration to the ROS2 topic.